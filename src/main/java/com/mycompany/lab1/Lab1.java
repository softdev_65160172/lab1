/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;

/**
 *
 * @author Ten
 */
public class Lab1 {
    private static char[][] board = new char[3][3]; //สร้าง board
    private static char XO = ' '; //ค่าเริ่มต้นของ x หรือ o
    private static char plays = ' ';//รับค่าคำตอบสำหรับเริ่มใหม่

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int position; // ตำแหน่ง
        boolean play = true; //กำหนดค่าไว้เริ่มเกมใหม่

while(play){ // loop เกม
        System.out.println("Welcome to OX Game");
        update(); //อัปเดท board
        printB(); //print board

    while (true) { //เลือก xo
                System.out.print("Choose X or O: ");
                String input = kb.next();
                if (input.equalsIgnoreCase("X") || input.equalsIgnoreCase("O")) {
                    XO = input.toUpperCase().charAt(0);
                    break;
                }
                System.out.println("Invalid input. Please choose X or O.");
            }

    while (true) { //รับค่าและเช็คแพ้ชนะ
        System.out.println("Player "+XO+" turn Enter a number from 1-9:");
        
        try{//ตรวจค่า position ให้เป็นตัวเลข
        position = kb.nextInt();
        if(checkmove(position)){
            move(position);
            printB();

            if (End()) {
                System.out.println("Game over! Player " + XO + " wins!");
                break;
            }else if (draw()) {
                System.out.println("It's a draw!");
                break;
            }

            if (XO == 'X') {
                XO = 'O';
            } else {
                XO = 'X';
            }

        }else{
            System.out.println("Invalid move! Try again.");
        }
        }catch(Exception e){
            System.out.println("Invalid input! Please enter a number.");
            kb.next();
        }             
    }    
    while(true) {//คำถามเริ่มเกมใหม่ตอนจบเกม
    System.out.print("Do you want to play again ? (Y/N): ");
        String playing = kb.next();
                if (playing.equalsIgnoreCase("Y") || playing.equalsIgnoreCase("N")) {
                    plays = playing.toUpperCase().charAt(0);
                    if(plays == 'N'){
                        play = false;
                        System.out.println("Thank you for playing our game.");
                    }break;
                }
                System.out.println("Invalid input. Please choose (Y/N)");
            }
    }
    }
    private static void update(){//อัปเดท board
        int count =1;
        for (int r = 0; r<3; r++){
            for (int c = 0; c<3; c++){
                board[r][c] =  Character.forDigit(count, 10);
                count++;
            }
        }
    }

    private static void move(int position){//ใส่ตำแหน่งของ position
        int r = (position-1)/3;
        int c = (position-1)%3;

        board[r][c] = XO;
    }

    private static boolean checkmove(int position){ //เช็คตำแหน่งไม่ให้ซ้ำกัน
        if (position <1 || position >9){
            return false;
        }
    int r = (position-1)/3;
    int c = (position-1)%3;

    return board[r][c] != 'X' && board[r][c] != 'O';
    } 
    
    private static void printB() { //print board
        System.out.println("-----------------");
        for (int r = 0; r<3; r++){
            for (int c = 0; c<3; c++){
                System.out.print("| ");
                System.out.print(board[r][c]+ " | ");
            }
            System.out.println("\n-----------------");
        }
    }

    private static boolean End() { //เช็คจบเกม
        // Check row
        for (int r = 0; r < 3; r++) {
            if (board[r][0] == XO && board[r][1] == XO && board[r][2] == XO) {
                return true;
            }
        }
        
        // Check col
        for (int c = 0; c < 3; c++) {
            if (board[0][c] == XO && board[1][c] == XO && board[2][c] == XO) {
                return true;
            }
        }
        
        // Check /
        if ((board[0][0] == XO && board[1][1] == XO && board[2][2] == XO) || (board[0][2] == XO && board[1][1] == XO && board[2][0] == XO)) {
            return true;
        }
        
        return false;
    }

    private static boolean draw() { // เช็คเสมอ
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (board[r][c] != 'X' && board[r][c] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

}    
